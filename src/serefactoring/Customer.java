package serefactoring;

import java.util.ArrayList;
import java.util.List;

class Customer {
    private String name;
    private List<Rental> rentals = new ArrayList<>();
    public Customer (String newName){
        name = newName;
    }
    public void addRental(Rental arg) {
        rentals.add(arg);
    }
    public String getName (){
        return name;
    }
    public String statement() {
        double totalAmount = 0;
        int frequentRenterPoints = 0;
        String result = "Rental Record for " + this.getName() + "\n";
        result += "\t" + "Title" + "\t" + "\t" + "Days" + "\t" + "Amount" + "\n";

        StringBuilder resultBuilder = new StringBuilder(result);

        for (Object current : rentals) {
            Rental currentRental = (Rental) current;

            double thisAmount = currentRental.getAmount();

            frequentRenterPoints = currentRental.addRenterPoints(frequentRenterPoints);

            //add figures for this rental
            resultBuilder.append("\t")
                    .append(currentRental.getMovie().getTitle())
                    .append("\t\t")
                    .append(currentRental.getDaysRented())
                    .append("\t")
                    .append(thisAmount)
                    .append("\n");
            totalAmount += thisAmount;
        }
        result = resultBuilder.toString();
        //add footer lines
        result += "Amount owed is " + totalAmount + "\n";
        result += "You earned " + frequentRenterPoints + " frequent renter points";
        return result;
    }


}
    