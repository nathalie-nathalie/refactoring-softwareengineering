package serefactoring;

import static org.junit.Assert.*;

import org.junit.Test;


public class TestRental {

    @Test
    public void testRentalAddRenterPointsRegular(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.REGULAR),2);
        int actual = testee.addRenterPoints(2);
        assertEquals(3, actual);
    }

    @Test
    public void testRentalAddRenterPointsChildren(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.CHILDRENS),2);
        int actual = testee.addRenterPoints(2);
        assertEquals(3, actual);
    }

    @Test
    public void testRentalAddRenterPointsNewReleaseWithBonus(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.NEW_RELEASE),2);
        int actual = testee.addRenterPoints(2);
        assertEquals(4, actual);
    }

    @Test
    public void testRentalAddRenterPointsNewReleaseWithoutBonus(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.NEW_RELEASE),1);
        int actual = testee.addRenterPoints(2);
        assertEquals(3, actual);
    }

    @Test
    public void testRentalGetAmountRegularWithoutExtraFees(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.REGULAR),2);
        double actual = testee.getAmount();
        assertEquals(2, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountRegularWithExtraFeesOneDay(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.REGULAR),3);
        double actual = testee.getAmount();
        assertEquals(3.5, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountRegularWithExtraFeesTwoDays(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.REGULAR),4);
        double actual = testee.getAmount();
        assertEquals(5.0, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountChildrenWithoutExtraFees(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.CHILDRENS),3);
        double actual = testee.getAmount();
        assertEquals(1.5, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountChildrenWithExtraFeesOneDay(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.CHILDRENS),4);
        double actual = testee.getAmount();
        assertEquals(3.0, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountChildrenWithExtraFeesTwoDays(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.CHILDRENS),5);
        double actual = testee.getAmount();
        assertEquals(4.5, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountNewReleaseOneDay(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.NEW_RELEASE),1);
        double actual = testee.getAmount();
        assertEquals(3.0, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountNewReleaseTwoDays(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.NEW_RELEASE),2);
        double actual = testee.getAmount();
        assertEquals(6.0, actual, 0.001);
    }

    @Test
    public void testRentalGetAmountNewReleaseFiveDays(){
        Rental testee = new Rental(new Movie("movie", PriceCategory.NEW_RELEASE),5);
        double actual = testee.getAmount();
        assertEquals(15.0, actual, 0.001);
    }
}
