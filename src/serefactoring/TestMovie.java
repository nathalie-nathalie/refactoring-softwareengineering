package serefactoring;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestMovie {
    @Test
    public void testMovieOneDayOverExtraFeesRegular(){
    Movie testee = new Movie("regular movie", PriceCategory.REGULAR);
    double extraFees = testee.getExtraFees(3);
    assertEquals(1.5, extraFees, 0.001);
    }

    @Test
    public void testMovieTwoDaysOverExtraFeesRegular(){
        Movie testee = new Movie("regular movie", PriceCategory.REGULAR);
        double extraFees = testee.getExtraFees(4);
        assertEquals(3.0, extraFees, 0.001);
    }

    @Test
    public void testMovieOneDayOverExtraFeesChildren(){
        Movie testee = new Movie("childrens movie", PriceCategory.CHILDRENS);
        double extraFees = testee.getExtraFees(4);
        assertEquals(1.5, extraFees, 0.001);
    }

    @Test
    public void testMovieTwoDaysOverExtraFeesChildren(){
        Movie testee = new Movie("childrens movie", PriceCategory.CHILDRENS);
        double extraFees = testee.getExtraFees(5);
        assertEquals(3.0, extraFees, 0.001);
    }

    @Test
    public void testMovieOneDayOverExtraFeesNewRelease(){
        Movie testee = new Movie("new release movie", PriceCategory.NEW_RELEASE);
        double extraFees = testee.getExtraFees(1);
        assertEquals(3.0, extraFees, 0.001);
    }

    @Test
    public void testMovieTwoDaysOverExtraFeesNewRelease(){
        Movie testee = new Movie("new release movie", PriceCategory.NEW_RELEASE);
        double extraFees = testee.getExtraFees(2);
        assertEquals(6.0, extraFees, 0.001);
    }

    @Test
    public void testMovieGetRegularFeesRegular(){
        Movie testee = new Movie("regular movie", PriceCategory.REGULAR);
        double regularFees = testee.getRegularFees();
        assertEquals(2.0, regularFees, 0.001);
    }

    @Test
    public void testMovieGetRegularFeesChildren(){
        Movie testee = new Movie("childrens movie", PriceCategory.CHILDRENS);
        double regularFees = testee.getRegularFees();
        assertEquals(1.5, regularFees, 0.001);
    }

    @Test
    public void testMovieGetRegularFeesNewRelease(){
        Movie testee = new Movie("new release movie", PriceCategory.NEW_RELEASE);
        double regularFees = testee.getRegularFees();
        assertEquals(0.0, regularFees, 0.001);
    }

    @Test
    public void testMovieExtraFeesDueRegular(){
        Movie testee = new Movie("regular movie", PriceCategory.REGULAR);
        boolean due = testee.extraFeesDue(3);
        assertTrue(due);
    }

    @Test
    public void testMovieExtraFeesNotDueRegular(){
        Movie testee = new Movie("regular movie", PriceCategory.REGULAR);
        boolean due = testee.extraFeesDue(2);
        assertFalse(due);
    }

    @Test
    public void testMovieExtraFeesDueChildren(){
        Movie testee = new Movie("regular movie", PriceCategory.CHILDRENS);
        boolean due = testee.extraFeesDue(4);
        assertTrue(due);
    }

    @Test
    public void testMovieExtraFeesNotDueChildren(){
        Movie testee = new Movie("regular movie", PriceCategory.CHILDRENS);
        boolean due = testee.extraFeesDue(3);
        assertFalse(due);
    }

    @Test
    public void testMovieExtraFeesDueNewRelease(){
        Movie testee = new Movie("regular movie", PriceCategory.NEW_RELEASE);
        boolean due = testee.extraFeesDue(1);
        assertTrue(due);
    }

}
