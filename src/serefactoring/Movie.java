package serefactoring;

public class Movie {
    private String title;
    public PriceCategory priceCategory;
    public Movie(String newTitle, PriceCategory newPriceCategory) {
        title = newTitle;
        priceCategory = newPriceCategory;
    }

    public String getTitle (){
        return title;
    }

    protected boolean extraFeesDue(int rentedDays){
        return rentedDays > priceCategory.getDaysUntilExtraFees();
    }

    protected double getExtraFees(int rentedDays){
        return (rentedDays - priceCategory.getDaysUntilExtraFees()) * priceCategory.getExtraFees();
    }

    protected double getRegularFees(){
        return  priceCategory.getFees();
    }

    protected int addBonus(int rentalDays){
        int points=0;
        if(priceCategory == PriceCategory.NEW_RELEASE && rentalDays > 1){
            points++;
        }
        return points;
    }

}