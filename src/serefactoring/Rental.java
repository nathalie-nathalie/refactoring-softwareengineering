package serefactoring;

class Rental {
    private Movie movie;
    private int daysRented;
    public Rental(Movie newMovie, int newDaysRented) {
        movie = newMovie;
        daysRented = newDaysRented;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int addRenterPoints(int renterPoints){
        return renterPoints + 1 + movie.addBonus(getDaysRented());
    }

    public double getAmount() {
        double thisAmount = movie.getRegularFees();
        if(movie.extraFeesDue(getDaysRented())){
            thisAmount += movie.getExtraFees(getDaysRented());
        }
        return thisAmount;
    }

}