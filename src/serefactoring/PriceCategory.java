package serefactoring;

public enum PriceCategory {

    REGULAR(2.0, 1.5, 2),
    NEW_RELEASE( 0, 3.0, 0),
    CHILDRENS(1.5, 1.5, 3);

    private double extraFees;
    private double fees;
    private int daysUntilExtraFees;

    PriceCategory(double fees, double extraFees, int daysUntilExtraFees){
        this.fees = fees;
        this.extraFees = extraFees;
        this.daysUntilExtraFees = daysUntilExtraFees;
    }

    protected double getExtraFees() { return extraFees; }

    protected double getFees(){return fees;}

    protected int getDaysUntilExtraFees() { return daysUntilExtraFees; }
}
